import Vue from 'vue'
import Router from 'vue-router'

//主界面
import Login from '../pages/login.vue'
import Main from '../pages/main.vue'
import WantAccess from '../pages/wantAccess.vue'

Vue.use(Router)

export default new Router({
      routes: [
        // 没有权限
        {
           path:"/WantAccess",
           name:"WantAccess",
           component:WantAccess,
        },
        //主界面
        {
          path:"/Main",
          name:"Main",
          component:Main,
       },
       // 登录页面
       {
        path:"/",
        name:"Login",
        component:Login,
        },
     ]
})
