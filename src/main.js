import Vue from 'vue'
import App from './App'
import router from './router'
import $ from 'jquery'
import iView from 'iView'
import VueResource from 'vue-resource';
import 'lib-flexible'
import 'iView/dist/styles/iview.css'

Vue.use(VueResource);
Vue.use(iView);
Vue.config.productionTip = false

// 初始化leanCloud
var APP_ID = '0v1RQvfpL4rrxEzKLpFvoXaD-gzGzoHsz';
var APP_KEY = '0wamBcaoj3PcFh37SKO3CYcz';
AV.init({
    appId: APP_ID,
    appKey: APP_KEY
});

new Vue({
     el: '#app',
     router,
     template: '<App/>',
     components: {App}
});
